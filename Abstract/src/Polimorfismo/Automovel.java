package Polimorfismo;

public class Automovel extends Veiculo{

	@Override
	protected void listarVerificacoes() {
		System.out.println("Tarefas a Realizar");
		ajustar();
		limpar();
		mudarOleo();
	}

	@Override
	protected void ajustar() {
		// TODO Auto-generated method stub
		System.out.println("Ajustando Amortecedor");
	}

	@Override
	public void limpar() {
		// TODO Auto-generated method stub
		System.out.println("Lavando com Sabonete Neutro");
	}
	
	protected void mudarOleo() {
		System.out.println("�leo Trocado com Sucesso! ");
	}

	public String toString() {
		return "Automovel";
	}
}
