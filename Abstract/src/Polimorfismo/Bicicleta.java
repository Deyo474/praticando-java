package Polimorfismo;

public class Bicicleta extends Veiculo {

	@Override
	public void listarVerificacoes() {
		System.out.println("Tarefas A realizar");
		ajustar();
		limpar();
	}

	@Override
	protected void ajustar() {
		// TODO Auto-generated method stub
		System.out.println("Ajustar pastilha");
	}

	@Override
	protected void limpar() {
		// TODO Auto-generated method stub
		System.out.println("Limpar Correia");
	}
	
	public String toString() {
		return "bike";
	}
}
