package Polimorfismo;

import java.util.Random;

public class Oficina{
	Random aleatorio = new Random();
	int par;
	Veiculo v;
	Automovel aut;
	Bicicleta bi;
	public Veiculo proximo() {
		par = aleatorio.nextInt(2 - 1 + 1 );
		if (par % 2 == 1 ) {
			return v = new Automovel();
		} else {
			return v = new Bicicleta();
		}
	}
	
	public void manutencao(Veiculo v) {
		v.listarVerificacoes();
	}
}
 