package Polimorfismo;

public abstract class Veiculo {
	
	 protected abstract void listarVerificacoes();
	 protected abstract void ajustar();
	 protected abstract void limpar();
}
	