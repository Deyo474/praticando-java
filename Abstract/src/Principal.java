
public class Principal {
	
	public static void main(String[] args) {
		
		Piriquito p = new Piriquito();
		Golfinho f = new Golfinho();
		
		p.setIdade(4);
		p.setNome("Devudu");
		
		f.setIdade(5);
		f.setNome("Thor");
		
		p.setVoa(true);
		p.setAssovia(true);
		p.setDancaAcasalamento(true);
		
		f.setMama(true);
		f.setProduzLeite(true);
		f.setQuadrupte(false);
		
		p.setDomestico(true);
		
		f.setAgua("Doce");
		f.setNada(true);
		
		System.out.println(f.fazBarulho());
		System.out.println(p.fazBarulho());
		
		//CRIAR CLASSE ABASTRATA AUTOMOVEL
		// COR,MARCA, MODELO, 
		// METODO ABS ACELERAR, PARAR, FAZER BARULHO
		// + 2 CLASSES CARRO / MOTO
		// 
	}
}
