
public class Corpo {
	private int linha, coluna;
	private String[][] matriz = { { "_", "_", "_",},
            { "_", "_", "_" },
            { "_", "_", "_" } };
	
	public void exibir() {
		System.out.println("---------------- Jogo da Velha ---------------- \n");
	    for (linha = 0; linha < matriz.length; linha++) {
	        for (coluna = 0; coluna < matriz[linha].length; coluna++) {
	            System.out.print(matriz[linha][coluna] + "|");
	        }
	        System.out.print("\n");
	    }
	}
	
	public void inserirMatriz(int linha, int coluna, String jogador) {
		matriz[linha][coluna] = jogador;
	}

	public int getLinha() {
		return linha;
	}

	public void setLinha(int linha) {
		this.linha = linha;
	}

	public int getColuna() {
		return coluna;
	}

	public void setColuna(int coluna) {
		this.coluna = coluna;
	}

	public String[][] getMatriz() {
		return matriz;
	}

	public void setMatriz(String[][] matriz) {
		this.matriz = matriz;
	}

	
}
