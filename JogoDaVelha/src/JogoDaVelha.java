
import java.util.Scanner;

public class JogoDaVelha {
	static String[][] matriz = new String[3][3];
	static int x ,y;
	static Scanner lin = new Scanner(System.in);
	static int jogadas = 1;
	static String jogador1;
	static String jogador2;
	public static void JogoDaVelha() {
		for (int l = 0; l < matriz.length; l++) {
			for (int c = 0; c < matriz[l].length; c++) { // Cria��o do Jogo com 3 linha e 3 colunas
				matriz[l][c] = "-";
			}
		}
	}

	public static void exibirJogo() {
		for (int l = 0; l < matriz.length; l++) {
			for (int c = 0; c < matriz[l].length; c++) {
				System.out.print(matriz[l][c] + "  "); // Exibindo o jogo na tela
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		JogoDaVelha();
		exibirJogo();
		
		
		
		System.out.println("Jogador 1 escolha uma letra");
		jogador1 = lin.nextLine();

		System.out.println("Jogador 2 escolha uma letra");
		jogador2 = lin.nextLine();
		while (jogadas < 8) {
			rodada();
			jogadas++;
		}
		
//

//        Scanner lin = new Scanner(System.in);
//        JogoDaVelha jogo = new JogoDaVelha();
//
//        jogo.exibirJogo();
//
//        System.out.println("Digite a Linha que deseja escolher");
//        int jogarLinha = lin.nextInt();
//
//        Scanner colun = new Scanner(System.in);
//        System.out.println("Digite a coluna que voc� deseja escolher");
//        int jogadorColuna = colun.nextInt();
//
//        //Usar a propriedade jogo para as perguntas e posi��es.
//        //como ?
//
//        if ( jogarLinha == 0 && jogadorColuna == 0) {
//            matriz[jogarLinha][jogadorColuna] = ("O");
//        } else if (jogarLinha == 0 && jogadorColuna == 1) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if ( jogarLinha == 0 && jogadorColuna == 2) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if (jogarLinha == 1 && jogadorColuna == 0) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if ( jogarLinha == 1 && jogadorColuna == 1) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if (jogarLinha == 1 && jogadorColuna == 2) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if ( jogarLinha == 2 && jogadorColuna == 0) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if (jogarLinha == 2 && jogadorColuna == 1) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if ( jogarLinha == 2 && jogadorColuna == 2) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        }
//        int c;
//        int l;
//        while (!VerificarJogadaValida(1, 0, 2)) {
//
//            if ( ! (VerificarJogadaValida(1, 0, 2))) {
//                System.out.println("Jogada Invalida tente novamente");
//            }
//
//            System.out.println("Digite a coluna que deseja marcar");
//            int coluna = colun.nextInt();
//
//            if ( !(VerificarJogadaValida(1, 0, 2))) {
//                System.out.println("Jogada invalida tente novamente");
//            }
//        }
//
//        jogo.exibirJogo();

	}

	public static void setJogada(int l, int c, String jogador) {
		if (verificarJogadaValida(l, c)) {
			matriz[l][c] = jogador;
		} else {
			System.out.println("posicao invalida digite uma linha e coluna novamente : ");
			x = lin.nextInt();
			y = lin.nextInt();
			setJogada(x, y, jogador);
		}
	}

	public static boolean verificarJogadaValida(int l, int c) {
		if ((l < 0) || (l > 2 )) {
			return false;
		} else if ((c < 0) || (c > 2 )) {
			return false;
		}
		if (matriz[l][c] != "-") {
			return false;
		}
		return true;
	}
	
	public  static void rodada() {
		if(jogadas%2==0) {
			mensagem(jogador2);
		}else {
			mensagem(jogador1);
		}
	}
	
	public static void mensagem(String jogador) {
		System.out.println("escolha linha e coluna : "+ jogador);
		x = lin.nextInt();
		y=lin.nextInt();
		setJogada(x, y,jogador );
		exibirJogo();
	}
}