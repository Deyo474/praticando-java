
public class Animais {

	private String nome = "Lobinho";
	private String cor = "amarelinho";
	private int idade = 8;
	private String raca = "leao";
	private boolean domestico = true;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public String getRaca() {
		return raca;
	}
	public void setRaca(String raca) {
		this.raca = raca;
	}
	public boolean isDomestico() {
		return domestico;
	}
	public void setDomestico(boolean domestico) {
		this.domestico = domestico;
	}
	
	
	
}
