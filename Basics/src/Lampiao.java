import java.util.Date;

public class Lampiao {
	
	public static void main(String[] args) {
		Lampada lampada = new Lampada();
		
		lampada.cor = "branca";
		lampada.potencia = 156;
		lampada.modelo = "A60";
		lampada.garantia = 36;
		lampada.tensao = 700;
		lampada.tipoLuz = "Amarela";
		lampada.tipoAbajur = true;
		lampada.preco = 5;
		lampada.consumoEnergia = 50;
		lampada.saldo = 10;
		lampada.dataFabricacao = new Date();  //Pegou a data Atual;
		
		lampada.tipos = new String[4];
		lampada.tipos[0] = "abajur";
		
		lampada.numeroFabricacao = new String[3];
		lampada.numeroFabricacao[0] = "47654678";
		lampada.numeroFabricacao[1] = "57546347";
		lampada.numeroFabricacao[2] = "46373474373";
		
		System.out.println(lampada.dataFabricacao); // imprime  a data atual;
		
		System.out.println(lampada.tipoAbajur);
		
		System.out.println(lampada.tipos[0]);
		
		System.out.println(lampada.numeroFabricacao[0]);
		System.out.println(lampada.numeroFabricacao[1]);
		System.out.println(lampada.numeroFabricacao[2]);
		
		lampada.exibirAutonomia();
		
		int obterAutonomia = lampada.obterAutonomia();
		//lampada.obterAutonomia();
			
		System.out.println("A autonomia da lampada � " + lampada.obterAutonomia());
		
		double qtdEnergia50 = lampada.calcularEnergia(50);
		double qtdEnergia100 = lampada.calcularEnergia(100);
		 
		System.out.println("Consudo de Energia " + qtdEnergia50);
		System.out.println("Consudo de Energia " + qtdEnergia100);
		
		lampada.ligaDesliga();
		lampada.mudarEstado();
		lampada.ligaDesliga();
		lampada.comprarLampada();
		
	}
}
