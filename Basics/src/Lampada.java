	import java.util.Date;
import java.util.Scanner;

public class Lampada {
	public Scanner ligada = new Scanner(System.in);
	
	
	int tensao;
	int potencia;
	int garantia;
	int preco;
	int consumoEnergia;
	int saldo;
	
	String modelo;
	String cor;
	String tipoLuz;
	String[] tipos;
	String[] numeroFabricacao;
	
	
	Date dataFabricacao;
	
	boolean tipoAbajur;
	boolean tipoLampeos;
	boolean tipoLuminarias;
	boolean liga = true;
	
	
	void exibirAutonomia() {
		int autonomia = potencia * tensao;
		if ( autonomia >= 300) {
			System.out.println("A tens�o � muito grande, tome cuidado");
		} else {
			System.out.println("A autonomia dessa Lampada �: " + tensao);
		}		
	}
	
	int obterAutonomia() {
		
		return tensao * potencia;
	}
	
	double calcularEnergia(double energia) {
			double qtdEnergia = energia / consumoEnergia;
			
			return qtdEnergia;
	}
	
	void ligaDesliga() {
		if (liga) {
			System.out.println("A lampada foi ligada");
			desligar();
			System.out.println("Lampada com estado alterado");
		} else {
			System.out.println("Lampada desligada");
		}
		//System.out.println("Lampada desligada");
	}
	
	void desligar () {
		liga = false;
	}
	
	void mudarEstado() {
		if (liga) {
			desligar();
		}
	}
	
	void comprarLampada() {
		int resto = saldo - preco;
		if( preco > saldo) {
			System.out.println("Voc� n�o pode comprar  a lampada seu saldo = " + saldo);
		} else if (preco < saldo) {
			System.out.println("Voc� pode comprar a lampada seu saldo = " + saldo + " Resta " +resto);
		}  else if (saldo == 0) {
			System.out.println("Voc� n�o possui saldo");
		}
	}
}
