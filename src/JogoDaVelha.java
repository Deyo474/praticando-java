
public class JogoDaVelha {
	static Corpo corpo = new Corpo();
	static Jogador jogador = new Jogador();
	static Regras regras = new Regras(corpo);
	
	public static void main(String[] args) {
		
		corpo.exibir();
		jogador.setSinal("X");
		
		regras.jogadaValida(0, 0, jogador);
		regras.jogada(null);
		
		jogador.setSinal("O");
		regras.jogadaValida(1, 1, jogador);
		regras.jogada(null);
		
		corpo.exibir();
	}
	
	
}
