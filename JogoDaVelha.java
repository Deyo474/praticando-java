import java.util.Scanner;

public class JogoDaVelha {
     static String[][] matriz = new String[3][3];

    public static void JogoDaVelha(){
        for (int l = 0; l < matriz.length; l++) {
            for (int c = 0; c < matriz [l].length; c ++) {   // Criação do Jogo com 3 linha e 3 colunas
                matriz[l][c] = String.valueOf(0);
            }
        }
    }
    public  static void exibirJogo() {
        for (int l = 0; l < matriz.length; l ++ ) {
            for ( int c = 0;  c < matriz[l].length; c ++) {
                System.out.println(matriz[l][c]+ "  " );  //Exibindo o jogo na tela
            }
            System.out.println();
        }
    }

    public static void main (String[] args) {
        JogoDaVelha();
        exibirJogo();
//

//        Scanner lin = new Scanner(System.in);
//        JogoDaVelha jogo = new JogoDaVelha();
//
//        jogo.exibirJogo();
//
//        System.out.println("Digite a Linha que deseja escolher");
//        int jogarLinha = lin.nextInt();
//
//        Scanner colun = new Scanner(System.in);
//        System.out.println("Digite a coluna que você deseja escolher");
//        int jogadorColuna = colun.nextInt();
//
//        //Usar a propriedade jogo para as perguntas e posições.
//        //como ?
//
//        if ( jogarLinha == 0 && jogadorColuna == 0) {
//            matriz[jogarLinha][jogadorColuna] = ("O");
//        } else if (jogarLinha == 0 && jogadorColuna == 1) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if ( jogarLinha == 0 && jogadorColuna == 2) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if (jogarLinha == 1 && jogadorColuna == 0) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if ( jogarLinha == 1 && jogadorColuna == 1) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if (jogarLinha == 1 && jogadorColuna == 2) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if ( jogarLinha == 2 && jogadorColuna == 0) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if (jogarLinha == 2 && jogadorColuna == 1) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        } else if ( jogarLinha == 2 && jogadorColuna == 2) {
//            matriz[jogarLinha][jogadorColuna] = "O";
//        }
//        int c;
//        int l;
//        while (!VerificarJogadaValida(1, 0, 2)) {
//
//            if ( ! (VerificarJogadaValida(1, 0, 2))) {
//                System.out.println("Jogada Invalida tente novamente");
//            }
//
//            System.out.println("Digite a coluna que deseja marcar");
//            int coluna = colun.nextInt();
//
//            if ( !(VerificarJogadaValida(1, 0, 2))) {
//                System.out.println("Jogada invalida tente novamente");
//            }
//        }
//
//        jogo.exibirJogo();

    }


    public static boolean VerificarJogadaValida(int jogadorX, int l, int c) {
        if ((l < 0) || ( l > 2)) {
            return false;
        }
        if ((c < 0) || (c > 2)) {
            return false;
        }                                        //Verificando se joguei menor que o ou maior que 2
        if (matriz[l][c] == "0" ) {                  //Como saber se jogou entre 0 e 2 ?
            return   false;
        }
        return true;
    }
}